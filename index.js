//On importe le module express
const express = require('express');
const app = express();
//On définit le port qui va nous permettre d'accéder au serveur
const port = 3000;

// On défini une route de type GET qui renvoie du texte en réponse 
app.get('/', (req, res) => {
    res.send('Hello World!');
})

// On défini une route de type GET qui renvoie un objet JSON en réponse 
app.get('/data' , (req, res) => {
    res.send({name: 'Pikachu', power: 20, life: 50});
})

//On démarre le serveur sur le port défini plus haut
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})